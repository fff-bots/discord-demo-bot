import discord
import datetime
import settings
import time
import requests

url = 'https://fff-api.dmho.de/v1/scrape/list'
response = requests.get(url)
response_json = response.json()


client = discord.Client()

@client.event
async def on_ready():
    print('We have logged in as {0.user}'.format(client))
    game = discord.Game("!demos")
    await client.change_presence(activity=game)

@client.event
async def on_message(message):
    if message.author == client.user:
        return

    if message.content.startswith('!demos') or message.content.startswith('Plan'):
        text = ""
        for city in response_json['list']:
            text = text + (city['city'] + "\n" + city['time'] + "\n" + city['place'] + "\n-----------------" + "\n")

        await message.channel.send(text)


client.run(settings.token)
